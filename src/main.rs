use dotrix::{Dotrix, Globals, World, Camera, Input, Pipeline, Transform,Frame, Color, Assets};
use dotrix::ecs::{Mut,System,RunLevel,Const, Entity};
use dotrix::math::{Point3, Vec3};
use dotrix::assets::{Mesh};
use dotrix::pbr::{self, Model, Material};

use rand::Rng;

use std::time::Instant;




const MIN_SCALE:f32=0.08;
const MAX_SCALE:f32=0.5;

const MIN_SPREAD:f32=-5.0;
const MAX_SPREAD:f32=5.0;


const SPAWN_INTERVAL:u8=2;

const AMOUNT:u8=3;

const DELETE_TIME:f32=3.5;



pub struct Data{
    pub snow_timer: u8,
}




pub struct Particle{
    pub spawn_time: Instant,
}


fn main() {

    Dotrix::application("System")
        // ############################ PARTICLE ############################ //
        .with_system(System::from(init).with(RunLevel::Startup))
        .with_system(System::from(print_fps).with(RunLevel::Standard))
        .with_system(System::from(spawn_particles).with(RunLevel::Standard))
        // .with_system(System::from(delete_particles).with(RunLevel::Standard))

        // ############################ CAMERA ############################ //
        .with_service(Camera{distance:10.5,
                            y_angle:0.0,
                            xz_angle:0.0,
                            target:Point3::new(0.0,0.0,0.0),
                            ..Default::default()}
                        )

        .with(pbr::extension)

        .run();
}


fn init(mut globals: Mut<Globals>, mut world: Mut<World>, mut assets:Mut<Assets>){
    //register particle mesh
    assets.import("./assets/particle.gltf");
    assets.register::<Mesh>("particle::mesh");

    //spawn global data struct
    globals.set(
        Data{
            snow_timer: 0,
        }
    );

    for _i in 0..AMOUNT{
        let rand_x_pos:f32=rand::thread_rng().gen_range(MIN_SPREAD..MAX_SPREAD);
        let rand_y_pos:f32=0.0;
        let rand_z_pos:f32=rand::thread_rng().gen_range(MIN_SPREAD..MAX_SPREAD);

        let rand_scale:f32=rand::thread_rng().gen_range(MIN_SCALE..MAX_SCALE);

        let rand_position:Vec3=Vec3::new(rand_x_pos, rand_y_pos, rand_z_pos);
        let rand_scale:Vec3=Vec3::new(rand_scale, rand_scale, rand_scale);
        world.spawn( Some((
            Model{mesh:assets.find::<Mesh>("particle::mesh").unwrap(),
                ..Default::default()
            },
            Material{albedo:Color::white(),
                ..Default::default()
            },
            Transform{translate:rand_position,
                    scale:rand_scale,
                ..Default::default()
            },
            Particle{spawn_time:Instant::now(),
            },
            Pipeline::default(),
            ),
        ));
    }

}

//FPS print
pub fn print_fps(frame:Mut<Frame>){
    println!("{:?}", frame.fps());
}

//ADDS PARTICLES TO WORLD
pub fn spawn_particles(mut globals: Mut<Globals>, mut world:Mut<World>, assets:Mut<Assets>, frame:Mut<Frame>){
    let mut timer:u8=0;

    if let Some(data) = globals.get_mut::<Data>() {
        if data.snow_timer > SPAWN_INTERVAL {
            data.snow_timer=0;
        } else {
            data.snow_timer += 1;
        }
        timer = data.snow_timer;
    }


    if timer == SPAWN_INTERVAL{
        let query = world.query::<(&mut Particle, &mut Transform)>();
        for (particle, transform) in query {
            let rand_x_pos:f32=rand::thread_rng().gen_range(MIN_SPREAD..MAX_SPREAD);
            let rand_y_pos:f32=0.0;
            let rand_z_pos:f32=rand::thread_rng().gen_range(MIN_SPREAD..MAX_SPREAD);

            let rand_scale:f32=rand::thread_rng().gen_range(MIN_SCALE..MAX_SCALE);

            let rand_position:Vec3=Vec3::new(rand_x_pos, rand_y_pos, rand_z_pos);
            let rand_scale:Vec3=Vec3::new(rand_scale, rand_scale, rand_scale);

            particle.spawn_time = Instant::now();
            transform.translate = rand_position;
            transform.scale = rand_scale;
        }
    }

}

pub fn delete_particles(mut world:Mut<World>){
    let mut remove_entity_vec:Vec<Entity>=vec![];
    for (ent, particle) in world.query::<(&mut Entity, &mut Particle,)>(){
        if Instant::elapsed(&particle.spawn_time).as_secs_f32()>=DELETE_TIME{
            remove_entity_vec.push(*ent);
        }
    }
    //DELETE FROM WORLD
    for e in remove_entity_vec{
        world.exile(e);
    }
}
